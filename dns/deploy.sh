#!/usr/bin/env bash

### AUTHORITATIVE SERVER

## Configuration de wiki.org 
# Configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp wiki.org/named.conf dwikiorg:/etc/.
hcp wiki.org/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 


## Configuration de iut.re 
# Configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 


## Configuration de pc1
# Fichier resolv.conf

hcp pc1/resolv.conf pc1:/etc/.

## Configuration de rt.iut.re
# Configuration du serveur DNS

himage drtiutre mkdir -p /etc/named
hcp rt-iut.re/named.conf drtiutre:/etc/.
hcp rt-iut.re/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 

## Configuration de pc2
# Fichier resolv.conf

hcp pc2/resolv.conf pc2:/etc/.


### TOP LEVEL DOMAIN

## Configuration de dorg
# Configuration du serveur DNS

himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 

## Configuration de dre
# Configuration du serveur DNS

himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf 

### ROOT SERVER

## Configuration de aRootServer
# Configuration du serveur DNS

himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf

## Arrêt du serveur DNS

himage diutre killall -9 named
himage drtiutre killall -9 named
himage dwikiorg killall -9 named
himage dorg killall -9 named
himage dre killall -9 named
himage aRootServer killall -9 named


## Démarrage du serveur DNS

himage diutre named -c /etc/named.conf
himage drtiutre named -c /etc/named.conf
himage dwikiorg named -c /etc/named.conf
himage dorg named -c /etc/named.conf
himage dre named -c /etc/named.conf
himage aRootServer named -c /etc/named.conf

